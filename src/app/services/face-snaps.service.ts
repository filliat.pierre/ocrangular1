import { Injectable } from "@angular/core";
import { FaceSnap } from "../models/face-snap.model";



@Injectable({
    providedIn: 'root'
})
export class FaceSnapsService {
    faceSnaps: FaceSnap[] = [
        {
          id: 1,  
          title: 'Archibald',
          description: "Mon meilleur ami depuis l'enfance",
          imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/bbc18909-eeaa-4656-8f07-0a5c57412dfb/df2h9xr-6a1ae0db-9676-44d0-bbeb-e5a84d0d266a.jpg/v1/fill/w_1024,h_576,q_75,strp/fishing_hub_platformer_by_olabukoo_df2h9xr-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NTc2IiwicGF0aCI6IlwvZlwvYmJjMTg5MDktZWVhYS00NjU2LThmMDctMGE1YzU3NDEyZGZiXC9kZjJoOXhyLTZhMWFlMGRiLTk2NzYtNDRkMC1iYmViLWU1YTg0ZDBkMjY2YS5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.vBTrJfP-aC73kCfZrZ2mZ5hC3ih_0qB9fb3GPizo1g8',
          createdDate: new Date (),
          snaps: 140,
          location: 'Paris'
        },
        {
          id: 2,
          title: 'New Dice',
          description: "Mes tout nouveaux dès pour le jeu de rôle",
          imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/25d406f6-c319-4263-866a-eba749641b19/dezrddx-fafd9f5f-a43b-4ff7-ab5b-cfb937370caa.jpg/v1/fill/w_1024,h_683,q_75,strp/paladin_s_oath_dice_set_by_thewizardsvault_dezrddx-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NjgzIiwicGF0aCI6IlwvZlwvMjVkNDA2ZjYtYzMxOS00MjYzLTg2NmEtZWJhNzQ5NjQxYjE5XC9kZXpyZGR4LWZhZmQ5ZjVmLWE0M2ItNGZmNy1hYjViLWNmYjkzNzM3MGNhYS5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.6ohmuZYXdh0l6IbnMJBShbjCRrTXAgH1Cvf92c4uLlY',
          createdDate: new Date (),
          snaps: 0
        },
        {
          id: 3,  
          title: 'Ouroboros',
          description: "Ouroboros et sa monture, le dragon Antharaxx",
          imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/3d079e1f-386b-4bd0-84fc-cce9913fbc0c/dezfy9r-17d44ac6-fc58-478f-800f-2cf194acc419.jpg/v1/fill/w_1920,h_1032,q_75,strp/the_witch_king_of_angmar_by_anatofinnstark_dezfy9r-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTAzMiIsInBhdGgiOiJcL2ZcLzNkMDc5ZTFmLTM4NmItNGJkMC04NGZjLWNjZTk5MTNmYmMwY1wvZGV6Znk5ci0xN2Q0NGFjNi1mYzU4LTQ3OGYtODAwZi0yY2YxOTRhY2M0MTkuanBnIiwid2lkdGgiOiI8PTE5MjAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.wzeTD-KJ6S6z13a1vd-4oVaujFZsE1p5TtD2FUjXC7M',
          createdDate: new Date(),
          snaps: 0,
          location: 'Coeur de Dréanne'
        },
        {
          id: 4,
          title: 'Archibald',
          description: "Mon meilleur ami depuis l'enfance",
          imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/bbc18909-eeaa-4656-8f07-0a5c57412dfb/df2h9xr-6a1ae0db-9676-44d0-bbeb-e5a84d0d266a.jpg/v1/fill/w_1024,h_576,q_75,strp/fishing_hub_platformer_by_olabukoo_df2h9xr-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NTc2IiwicGF0aCI6IlwvZlwvYmJjMTg5MDktZWVhYS00NjU2LThmMDctMGE1YzU3NDEyZGZiXC9kZjJoOXhyLTZhMWFlMGRiLTk2NzYtNDRkMC1iYmViLWU1YTg0ZDBkMjY2YS5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.vBTrJfP-aC73kCfZrZ2mZ5hC3ih_0qB9fb3GPizo1g8',
          createdDate: new Date (),
          snaps: 350,
          location: 'Paris'
        },
        {
          id: 5,
          title: 'New Dice',
          description: "Mes tout nouveaux dès pour le jeu de rôle",
          imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/25d406f6-c319-4263-866a-eba749641b19/dezrddx-fafd9f5f-a43b-4ff7-ab5b-cfb937370caa.jpg/v1/fill/w_1024,h_683,q_75,strp/paladin_s_oath_dice_set_by_thewizardsvault_dezrddx-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NjgzIiwicGF0aCI6IlwvZlwvMjVkNDA2ZjYtYzMxOS00MjYzLTg2NmEtZWJhNzQ5NjQxYjE5XC9kZXpyZGR4LWZhZmQ5ZjVmLWE0M2ItNGZmNy1hYjViLWNmYjkzNzM3MGNhYS5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.6ohmuZYXdh0l6IbnMJBShbjCRrTXAgH1Cvf92c4uLlY',
          createdDate: new Date (),
          snaps: 0
        },
        {
          id: 6,
          title: 'Ouroboros',
          description: "Ouroboros et sa monture, le dragon Antharaxx",
          imageUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/3d079e1f-386b-4bd0-84fc-cce9913fbc0c/dezfy9r-17d44ac6-fc58-478f-800f-2cf194acc419.jpg/v1/fill/w_1920,h_1032,q_75,strp/the_witch_king_of_angmar_by_anatofinnstark_dezfy9r-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTAzMiIsInBhdGgiOiJcL2ZcLzNkMDc5ZTFmLTM4NmItNGJkMC04NGZjLWNjZTk5MTNmYmMwY1wvZGV6Znk5ci0xN2Q0NGFjNi1mYzU4LTQ3OGYtODAwZi0yY2YxOTRhY2M0MTkuanBnIiwid2lkdGgiOiI8PTE5MjAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.wzeTD-KJ6S6z13a1vd-4oVaujFZsE1p5TtD2FUjXC7M',
          createdDate: new Date(),
          snaps: 0,
          location: 'Coeur de Dréanne'
        }
    ];

    getAllFaceSnaps(): FaceSnap[]{
        return this.faceSnaps;
    }

    getFaceSnapById(faceSnapId: number): FaceSnap {
        const faceSnap = this.faceSnaps.find(faceSnap => faceSnap.id === faceSnapId);
        if (!faceSnap) {
            throw new Error('FaceSnap not found!');
        } else {
            return faceSnap
        }

    }
    snapFaceSnapById(faceSnapId: number, snapType: 'snap' | 'unsnap'): void {
        const faceSnap = this.getFaceSnapById(faceSnapId);
        snapType === 'snap' ? faceSnap.snaps++ : faceSnap.snaps--;
    }
}
